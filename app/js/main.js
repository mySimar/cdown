$(document).ready(function () {
    /*$(".notify").find("#submit").on("click", function(e) {
        e.preventDefault();
    });*/
    $('#countdown').countdown({
        date: '12/24/2019 23:59:59'
    }, function () {
        console.log('Merry Christmas!');
    });
    $(".menu-handler").on('click', function () {
        $(this).parent().toggleClass("menu-open");
        /*if ( $(this).parent().hasClass('menu-open') ) {
            $(this).parent().removeClass('menu-open');
        } else {
            $(this).parent().addClass('menu-open');
        }*/
    });
    $("#menu a").eq(0).on('click', function () {
        /*var homePage = $(this).parent().parent().parent(); //<nav>
        if(homePage.hasClass("menu-open")) {
            homePage.toggleClass("menu-open");
        }*/
        var homePage = $(this).parent().parent().parent();
        var teamPage = $(this).closest("main");
        if(homePage.hasClass("menu-open")) {
            homePage.toggleClass("menu-open");
            teamPage.find("#team").hide().fadeOut(800);
            teamPage.find("#contact").hide().fadeOut(800);
            teamPage.find(".content").show().fadeIn(800);
        }
    });
    $("#menu a").eq(1).on('click', function () {
        var homePage = $(this).parent().parent().parent();
        var teamPage = $(this).closest("main");
        if(homePage.hasClass("menu-open")) {
            homePage.toggleClass("menu-open");
            teamPage.find(".content").hide().fadeOut(800);
            teamPage.find("#contact").hide().fadeOut(800);
            teamPage.find("#team").toggle().fadeIn(800);
        }
    });
    $("#menu a").eq(2).on('click', function () {
        var homePage = $(this).parent().parent().parent();
        var teamPage = $(this).closest("main");
        if(homePage.hasClass("menu-open")) {
            homePage.toggleClass("menu-open");
            teamPage.find(".content").hide().fadeOut(800);
            teamPage.find("#team").hide().fadeOut(800);
            teamPage.find("#contact").toggle().fadeIn(800);
        }
    });
});

/*====================================
 Preloader
 ======================================*/
$(window).load(function() {
    var preloaderDelay = 350,
        preloaderFadeOutTime = 500;
    function hidePreloader() {
        var loadingAnimation = $('.loader'),
            preloader = $('#preloader');
        loadingAnimation.fadeOut();
        preloader.delay(preloaderDelay).fadeOut(preloaderFadeOutTime);
    }
    hidePreloader();
});